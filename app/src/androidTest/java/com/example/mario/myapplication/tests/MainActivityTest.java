package com.example.mario.myapplication.tests;


import android.content.Intent;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;
import android.widget.TextView;
import junit.framework.JUnit4TestAdapter;


import com.example.mario.myapplication.MainActivity;
import com.example.mario.myapplication.R;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class MainActivityTest extends
            android.test.ActivityUnitTestCase<MainActivity> {

        private MainActivity activity;

        public MainActivityTest() {
            super(MainActivity.class);
        }
		
		public static junit.framework.Test suite() { 
			return new JUnit4TestAdapter(MainActivityTest.class); 
		}

        @Override
        protected void setUp() throws Exception {
            super.setUp();
            Intent intent = new Intent(getInstrumentation().getTargetContext(),
                    MainActivity.class);
            startActivity(intent, null, null);
            activity = getActivity();
        }

        @Test
        public void testSomething() {
            TextView text = (TextView)activity.findViewById(R.id.hello);
            assertEquals("Hello world!", text.getText());
        }
		
		@Test
        public void testSomethingFail() {
            TextView text = (TextView)activity.findViewById(R.id.hello);
            assertEquals("Hello world!", text.getText());
        }

        @Test
        public void testClickOnButton() {
            Button btn = (Button)activity.findViewById(R.id.changeTextButton);
            btn.performClick();

            TextView text = (TextView)activity.findViewById(R.id.hello);
            assertEquals("Hallo Welt!", text.getText());
        }


    @Test
        public void testSumthingRight() {
            int sum = activity.sum(3,4);
            assertSame(sum, 7);
        }

        @Override
        protected void tearDown() throws Exception {
            super.tearDown();
        }
    }

